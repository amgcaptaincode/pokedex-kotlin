package com.amg.pokedex

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.lang.ClassCastException

class ListFragment : Fragment() {

    lateinit var pokemonSelectListener: PokemonSelectListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        pokemonSelectListener = try {
            context as PokemonSelectListener
        } catch (e : ClassCastException) {
            throw ClassCastException("$context must implemented PokemonSelectListener")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_list, container, false)
        val rvPokemon = rootView.findViewById<RecyclerView>(R.id. rv_pokemon)
        rvPokemon.layoutManager = LinearLayoutManager(requireActivity())

        val pokemonAdapter = PokemonAdapter()
        rvPokemon.adapter = pokemonAdapter

        pokemonAdapter.onItemClickListener = {
            pokemonSelectListener.onPokemonSelected(it)
        }

        val pokemonList: MutableList<Pokemon> = mutableListOf(
            Pokemon(7, "Squirtle", 35, 55, 40, 80, Pokemon.PokemonType.WATER),
            Pokemon(5, "Charmeleon", 48, 33, 40, 80, Pokemon.PokemonType.FIRE),
            Pokemon(25, "Pikachu", 31, 44, 40, 80, Pokemon.PokemonType.ELECTRIC),
            Pokemon(56, "Mankey", 88, 55, 40, 80, Pokemon.PokemonType.FIGHTER),
            Pokemon(6, "Charizard", 36, 55, 40, 80, Pokemon.PokemonType.FIRE),
            Pokemon(43, "Oddish", 87, 55, 40, 80, Pokemon.PokemonType.GRASS),
            Pokemon(9, "Blastoise", 63, 55, 40, 80, Pokemon.PokemonType.WATER),
            Pokemon(26, "Raichu", 46, 55, 40, 80, Pokemon.PokemonType.ELECTRIC),
            Pokemon(54, "Psyduck", 77, 55, 40, 80, Pokemon.PokemonType.WATER),
            Pokemon(59, "Arcanine", 16, 55, 40, 80, Pokemon.PokemonType.FIGHTER),
            Pokemon(10, "Caterpie", 17, 55, 40, 80, Pokemon.PokemonType.GRASS),
            Pokemon(66, "Machop", 44, 55, 40, 80, Pokemon.PokemonType.FIGHTER)
        )

        pokemonAdapter.submitList(pokemonList)

        return rootView;
    }

    interface PokemonSelectListener {
        fun onPokemonSelected(pokemon: Pokemon)
    }

}