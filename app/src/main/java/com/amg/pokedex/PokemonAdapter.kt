package com.amg.pokedex

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class PokemonAdapter : ListAdapter<Pokemon, PokemonAdapter.PokemonViewHolder>(DiffCallbacks) {

    companion object DiffCallbacks : DiffUtil.ItemCallback<Pokemon>() {
        override fun areItemsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Pokemon, newItem: Pokemon): Boolean {
            return oldItem == newItem
        }

    }

    lateinit var onItemClickListener : (Pokemon) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_list_item, parent, false)
        return PokemonViewHolder(view)
    }

    override fun onBindViewHolder(holder: PokemonViewHolder, position: Int) {
        val pokemon = getItem(position)
        holder.bind(pokemon)
    }

    inner class PokemonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvPokemonId = itemView.findViewById<TextView>(R.id.tv_pokemon_id)
        private val tvPokemonName = itemView.findViewById<TextView>(R.id.tv_pokemon_name)
        private val ivPokemonType = itemView.findViewById<ImageView>(R.id.iv_pokemon_type)

        fun bind(pokemon: Pokemon) {

            tvPokemonId.text = pokemon.id.toString()
            tvPokemonName.text = pokemon.name

            val imageId = when (pokemon.type) {
                Pokemon.PokemonType.GRASS -> R.drawable.frass
                Pokemon.PokemonType.FIRE -> R.drawable.fire
                Pokemon.PokemonType.WATER -> R.drawable.water
                Pokemon.PokemonType.FIGHTER -> R.drawable.fight
                Pokemon.PokemonType.ELECTRIC -> R.drawable.electric
            }

            ivPokemonType.setImageResource(imageId)

            itemView.setOnClickListener {
                if (::onItemClickListener.isInitialized) {
                    onItemClickListener(pokemon)
                }
            }

        }

    }
}