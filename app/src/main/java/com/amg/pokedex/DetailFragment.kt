package com.amg.pokedex

import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

class DetailFragment : Fragment() {

    private lateinit var ivPokemon : ImageView
    private lateinit var tvPokemonHp : TextView
    private lateinit var tvPokemonAttack : TextView
    private lateinit var tvPokemonDefense : TextView
    private lateinit var tvPokemonSpeed : TextView
    private lateinit var progreesWheel : ProgressBar

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val  rootView = inflater.inflate(R.layout.fragment_detail, container, false)

        ivPokemon = rootView.findViewById(R.id.iv_detail)
        tvPokemonHp = rootView.findViewById(R.id.tv_pokemon_hp)
        tvPokemonAttack = rootView.findViewById(R.id.tv_pokemon_attack)
        tvPokemonDefense = rootView.findViewById(R.id.tv_pokemon_defense)
        tvPokemonSpeed = rootView.findViewById(R.id.tv_pokemon_speed)
        progreesWheel = rootView.findViewById(R.id.progress_wheel)

        return rootView
    }

    fun setPokemonData(pokemon: Pokemon) {
        progreesWheel.visibility = View.VISIBLE
        val idString: String = if (pokemon.id >= 10) {
            "0${pokemon.id}"
        } else {
            "00${pokemon.id}"
        }

        Glide.with(this)
            .load("https://assets.pokemon.com/assets/cms2/img/pokedex/full/$idString.png")
            .listener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    progreesWheel.visibility = View.GONE
                    ivPokemon.setImageResource(R.drawable.ic_broken_image)
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    progreesWheel.visibility = View.GONE
                    return false
                }
            })
            .error(R.drawable.ic_broken_image)
            .into(ivPokemon)

        tvPokemonHp.text = getString(R.string.text_hp_format, pokemon.hp)
        tvPokemonAttack.text = getString(R.string.text_attack_format, pokemon.attack)
        tvPokemonDefense.text = getString(R.string.text_defense_format, pokemon.defense)
        tvPokemonSpeed.text = getString(R.string.text_speed_format, pokemon.speed)
    }

}